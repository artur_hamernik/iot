import React, {Component} from 'react';
import { Text, View, StyleSheet,TouchableOpacity, ScrollView, TextInput, Image} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import Icon from 'react-native-ionicons'
import { ColorPicker } from 'react-native-color-picker'
import AsyncStorage from '@react-native-async-storage/async-storage'
import SplashScreen from 'react-native-splash-screen';

const KEY = '@key_devices';

class DevicesScreen extends Component {
  constructor(props){
    super(props);
    this.state = {
      navigation: this.props.navigation,
      devices: []
    };
  }
  async componentDidMount(){
    this.getData();
  }
  async storeData(){
      try {
          console.log("storing data")
          const devices = this.state.devices
          console.log(devices);
          await AsyncStorage.setItem(KEY, JSON.stringify(devices));
      } catch (err) {
          console.log(err);
      }
  };

  async getData(){
      try {
        console.log("getting data")
          const value = await AsyncStorage.getItem(KEY);
          if (value !== null) {
              this.setState({devices: JSON.parse(value)})
          }
      } catch (err) {
          console.log(err);
      }
  };
  render(){
    var device = this.props.route.params.device;
    console.log(device)
    const navigation = this.state.navigation;
    var devices = this.state.devices;
    if(device !== 'none'){
      devices.push(this.props.route.params.device)
      this.storeData()
    }
    return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <View style={styles.toolbar}>
            <View style={{ flex : 1 }}></View>
            <Text style={[{ color:"black", fontSize:26 },styles.Roboto]}>Devices</Text>
            <View style={{ flex : 1 }}></View>
          </View>
          <View style={{flex:10, justifyContent:'flex-start', alignItems:'flex-start'}}>
            <ScrollView
            nestedScrollEnabled={true}
            contentContainerStyle={{
                flex: 1,
                flexWrap: "wrap",
                flexDirection: 'row'
              }} style={{flex:1, flexDirection:'row', paddingLeft: '6%', paddingRight: '6%', flexWrap:'wrap', }}>
              {
                devices.map(n=>(
                  <View style={[styles.devicebox,{backgroundColor:n.color}]}>
                    <Text style={{fontSize: 22}}>{n.name}</Text>
                    <Text>{n.place}</Text>
                  </View>
                ))
              }
            <TouchableOpacity style={styles.devicebox}
              onPress={()=>{navigation.navigate('New Device')}}>
              <Image
                style={styles.stretch}
                source={require('./plus2.png')}
              />
            </TouchableOpacity>
            </ScrollView>
          </View>
        </View>
    );
}
}

class ConnectionScreen extends Component {
  constructor(props){
    super(props);
    this.state = {
    };
  }
  render(){
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <Text>Settings!</Text>
        <Icon name="md-bluetooth"/>
      </View>
    );
  }
}

class NewDeviceScreen extends Component {
  constructor(props){
    super(props);
    this.state = {
      navigation: this.props.navigation,
      valueName: 'Name',
      valuePlace: 'Place',
      valueCommand: 'Command',
      currentColor: 'D3D3D3'
    };
  }
  onChangeText(text,id){
    if(id === 1){
      this.setState({valueName: text})
    }
    else if(id === 2){
      this.setState({valuePlace: text})
    }
    else if(id === 3){
      this.setState({valueCommand: text})
    }
  }
  render(){
    const navigation = this.state.navigation;
    return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>

          <View style={styles.toolbar}>
            <View style={{ flex : 1 }}></View>
            <Text style={[{ color:"black", fontSize:26 },styles.Roboto]}>New Device</Text>
            <View style={{ flex : 1 }}></View>
          </View>
          <View style={{flex:10, justifyContent:'flex-start', alignItems:'center', flexDirection: 'column', flexWrap: 'wrap', paddingLeft: '1%', paddingRight: '1%'}}>
          <ScrollView>
              <TextInput
                  style={styles.textInput}
                  onChangeText={text => this.onChangeText(text,1)}
                  placeholder="Name"
                  placeholderTextColor="grey"
              />
              <TextInput
                  style={styles.textInput}
                  onChangeText={text => this.onChangeText(text,2)}
                  placeholder="Place"
                  placeholderTextColor="grey"
              />
              <TextInput
                  style={styles.textInput}
                  onChangeText={text => this.onChangeText(text,3)}
                  placeholder="Command"
                  placeholderTextColor="grey"
              />
              <Text style={{fontSize: 24, alignSelf: 'center'}}>Color</Text>
              <View style={{width: 340, height: 150, borderColor: 'black', borderWidth: 1, backgroundColor:'white'}}>
              <ColorPicker
                  onColorSelected={color => this.setState({currentColor: color})}
                  style={{flex: 1}}
                />
              </View>
              <View style={{flex:2, flexDirection: 'row', paddingVertical: 40, marginHorizontal:'8%'}}>
                <TouchableOpacity style={styles.cancelSave} onPress={()=>{
                  navigation.navigate("Devices");
                }}><Text>Cancel</Text></TouchableOpacity>
                <TouchableOpacity style={styles.cancelSave} onPress={()=>{
                  console.log(this.state.currentColor)
                  navigation.navigate("Devices", {device: {name: this.state.valueName, place: this.state.valuePlace, color: this.state.currentColor, command: this.state.valueCommand}})}}><Text>Save</Text></TouchableOpacity>
              </View>
          </ScrollView>

        </View>
        </View>
    );
  }
}

const Tab = createBottomTabNavigator();

const DevicesStack = createStackNavigator();

class MainTabScreen extends Component {
  constructor(props){
    super(props);
    this.state = {
    };
  }
  componentDidMount(){

  }

  render(){
    return (
      <Tab.Navigator
        screenOptions={({ route }) => ({
          tabBarIcon: ({ focused, color, size }) => {
            let iconName;

            if (route.name === 'Devices') {
              iconName = 'ios-home'
            } else if (route.name === 'Connection') {
              iconName = 'md-bluetooth';
            }

            // You can return any component that you like here!
            return <Icon name={iconName} size={size} color={color}/>
            //<Ionicons name={iconName} size={size} color={color} />;
          },
        })}
        tabBarOptions={{
          activeTintColor: 'tomato',
          inactiveTintColor: 'gray',
        }}
      >
        <Tab.Screen name="Devices" component={DevicesScreen} initialParams={{device:'none'}}/>
        <Tab.Screen name="Connection" component={ConnectionScreen}/>
      </Tab.Navigator>
    );
  }
}

export default class App extends Component {
  constructor(props){
    super(props);
    this.state = {
    };
    SplashScreen.hide();
  }
  render(){
  return (
    <NavigationContainer>
      <DevicesStack.Navigator mode="modal">
        <DevicesStack.Screen name="Main" component={MainTabScreen} options={{ headerShown: false }}/>
        <DevicesStack.Screen name="New Device" component={NewDeviceScreen} options={{ headerShown: false }}/>
      </DevicesStack.Navigator>
    </NavigationContainer>
  );
}
}

const styles = StyleSheet.create({
  thumb: {
       width: 20,
       height: 20,
       borderColor: 'white',
       borderWidth: 1,
       borderRadius: 10,
       shadowColor: 'black',
       shadowOffset: {
           width: 0,
           height: 2
       },
       shadowRadius: 2,
       shadowOpacity: 0.35,
    },
  devicebox:{width: 160, height: 160, borderColor:'black', borderWidth: 1, alignItems:'center', justifyContent:'center', padding : 10, backgroundColor: 'lightgrey', margin :10, alignSelf: 'flex-start'},
  cancelSave:{flex:0.5, borderColor:'black', borderWidth: 1, alignItems:'center', justifyContent:'center', padding : 10, backgroundColor: 'lightgrey', marginHorizontal: 20},
  textInput:{ width: 340, marginVertical: 20, height: 40, borderColor: 'gray', borderWidth: 1 },
  container: {
    flex: 1
  },
  toolbar:{
    flex: 1,
    borderColor:"black",
    borderBottomWidth: 1,
    flexDirection:"row",
    alignItems:"center",
    paddingLeft: 15,
  },
  font22:{
    textAlign:"justify",
    paddingLeft: 30,
    paddingRight: 30,
    fontSize: 22,
    marginBottom: 10
  },
  font16:{
    fontSize: 16
  },
  title: {
    fontSize: 18,
  },

  Roboto:{
    fontFamily: "Roboto-Black"
  },
  Lato:{
    fontFamily: "Lato-Regular"
  },
  stretch: {
    width: 100,
    height: 100,
    resizeMode: 'stretch'
  }
});
